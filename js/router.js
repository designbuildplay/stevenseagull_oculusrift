
// ========
// ROUTER : 
// ========

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES :::::::::::::::::::

    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Menu     = require('view.menu'),
        End     = require('view.end'),
        Game     = require('view.game');
        

    //var view_questions = new QuestionsList({collection: Questions});

    //var questions_collection = new Questions();
    //console.log("coll is ", Questions.models[0])
    // //console.log(view_questions.collection.models)
    // view_questions.collection.models[0].set({"char":"char1"});

   //define router class
    var AppRouter = Backbone.Router.extend ({
        routes: {
            '' : 'menu',
            'menu' : 'menu',
            'game': 'game',
            'end': 'end',
        },

        home: function () {
            console.log('you are viewing home page');
           // var welcome_view = new Welcome(); // WELCOME SCREEN
        },
        
        menu: function () {
            console.log('you are viewing MENU');
            var menu_view = new Menu();
        },

        game: function () {
            console.log('you are viewing THE GAME');
            var game_view = new Game();
        },

        end: function () {
            console.log('you are viewing GAME OVER');
            var end_view = new End();
            //view_questions.render(0); // call render funct with model reference ID
        },



        initialize: function(){
            console.log("route this thing")
        }

    });

  return AppRouter

});
