// THE INTRO VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),

        projectTemplate    = require("text!templates/menu.html");

    //var scope = this;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var MenuView = Backbone.View.extend({

        tagName:'div',
        id:"menu",    
        el:'#viewport',  //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
           'click #btn-begin': 'startClick',
        },

        initialize:function () {
              console.log("view active Menu" )
              this.render();
        },
      
        render:function () {
            this.$el.html(this.template());
            //console.log(    this.$el.html() )
            return this;
        },

        startClick:function () {
            var Router = require('router'),
                appRouter = new Router(); 

            appRouter.navigate('game', {trigger: true}); // TRIGGERS THE PAGE FROM ROUTER
        }
    });


    // Our module now returns our view
    return MenuView;

});
