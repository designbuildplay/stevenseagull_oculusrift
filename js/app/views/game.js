// THE INTRO VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        //Game             = require('game'),
       Game        = require('seagullGame'),

        projectTemplate    = require("text!templates/game.html");

    //var scope = this;

    // CONTENT :::::::::::::::::::::::::::::::::::

    var GameView = Backbone.View.extend({

        tagName:'div',
        id:"game",    
        el:'#viewport',   //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name

        events: {
            //'click .btn-welcome': 'welcomeClick',
        },

        initialize:function () {
              console.log("view active Game" )
              this.render();
        },
      
       
        render:function () {
            this.$el.html(this.template());
            //var seagullGame = new Game(); //Game()
            var seagullGame = new Game(); //
            return this;
        }

        
    });


    // Our module now returns our view
    return GameView;

});
