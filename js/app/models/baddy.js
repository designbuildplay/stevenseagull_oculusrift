define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore');

    // CONTENT :::::::::::::::::::::::::::::::::::
    
    var Question = Backbone.Model.extend({

      defaults: {
        id: "", 
        title:"", 
        desc:"",
        q1:"",
        q2:"",
        q3:"",
        character:"",
        answer:null
      },
      
      initialize: function(){
        
        console.log(this.get('title') + ' has been initialized.');
        
        // listens for change update  
        this.on('change', function(){
            console.log(this.get('title') + '- Values for this model have changed.');
        });
    }

    });

    return Question

});