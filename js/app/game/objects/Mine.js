define(function (require) {
	
	var  	ColladaLoader 		= require('collada'),
		    Detector            = require('detector'),
		    THREE			    = require('threeCore');

	var Mine = function(){
		console.log("BOMB!")
		var scope = this;
		this.mesh = {};

		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use
		//this.dae = new THREE.Object3D();//create an empty container
		this.dae = {};

		var loader = new THREE.OBJMTLLoader();
				loader.load( 'models/dae/mine.obj', 'models/dae/mine.mtl', function ( object ) {

					object.position.y = - 80;
					scope.dae = object;
					//scene.add( object );


					scope.dae.scale.x = scope.dae.scale.y = scope.dae.scale.z =  Math.random() * 4;
					scope.dae.position.x =  Math.random() * 20000 - 10000;
					scope.dae.position.z = Math.random() * 20000 - 10000;
					scope.dae.position.y =  Math.random() * 900 + 160;
					scope.dae.rotation.x =  Math.random() * 560;

					//console.log('mine ', scope.dae)
					scope.loaded = true;
					scope.active = true;

		} );

		
	}

	return Mine
});
