define(function (require) {
	
	var  	ColladaLoader 		= require('collada'),
			OBJMTLLoader		= require('OBJMTLLoader'),
			OBJLoader 			= require('OBJLoader'),	
		    Detector            = require('detector'),
		    THREE			    = require('threeCore');

	var Mine = function(){
		//console.log("CRAB!")
		var scope = this;
		this.mesh = {};

		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use
		//this.dae = new THREE.Object3D();//create an empty container
		this.dae = {};


		var loader = new THREE.OBJMTLLoader();
				loader.load( 'models/dae/sun.obj', 'models/dae/sun.mtl', function ( object ) {

					object.position.y = - 80;
					scope.dae = object;
					//scene.add( object );


					scope.dae.scale.x = scope.dae.scale.y = scope.dae.scale.z =  11;
					scope.dae.position.x = 1000;
					//scope.dae.position.z = Math.random() * 10000;
					scope.dae.position.y =  6200;

					//console.log('mine ', scope.dae)
					scope.loaded = true;
					scope.active = true;
					console.log("SUNNY!")
		} );

	

		
	}

	return Mine
});
