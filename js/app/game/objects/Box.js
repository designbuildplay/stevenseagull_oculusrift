define(function (require) {
		
	var  	THREE			    = require('threeCore');
		  
	var Box = function(){

		var scope = this;
		this.mesh = {};
		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use

		var geometry = new THREE.CubeGeometry( 200, 200, 200 );
		this.texture = THREE.ImageUtils.loadTexture( 'textures/crate.gif' );
		//texture.anisotropy = renderer.getMaxAnisotropy();
		var material = new THREE.MeshBasicMaterial( { map: this.texture } );
		this.mesh = new THREE.Mesh( geometry, material );

		this.mesh.scale.x = this.mesh.scale.y = this.mesh.scale.z =  Math.random() * 2;
		this.mesh.position.x =  Math.random() * 20000 - 10000;
		this.mesh.position.z = Math.random() * 20000 - 10000;
		this.mesh.position.y =  Math.random() * 700 + 160;
		this.mesh.rotation.x =  Math.random() * 360;

		// this.mesh.position.x = 800;
		// this.mesh.position.y = 260;
		this.loaded = true;
		this.active = true;
	}

	return Box
});
