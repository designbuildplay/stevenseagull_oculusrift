define(function (require) {
	
	var  	ColladaLoader 		= require('collada'),
			OBJMTLLoader		= require('OBJMTLLoader'),
			OBJLoader 			= require('OBJLoader'),	
		    Detector            = require('detector'),
		    THREE			    = require('threeCore');

	var Mine = function(){
		console.log("PIRATE SHIP!")
		var scope = this;
		this.mesh = {};

		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use
		//this.group = new THREE.Object3D();//create an empty container
		this.dae = {};
		this.speed  = Math.random() * 30;
		this.HitMesh;
		this.collide = false;

		var loader = new THREE.OBJMTLLoader();
				loader.load( 'models/dae/pirate_ship.obj', 'models/dae/pirate_ship.mtl', function ( object ) {

					object.position.y = - 80;
					scope.dae = object;
					//scene.add( object );


					scope.dae.scale.x = scope.dae.scale.y = scope.dae.scale.z =  100;
					scope.dae.position.x =  Math.random() * 15000 - 5000;
					scope.dae.position.z = Math.random() *  15000 - 5000;
					scope.dae.position.y =  -140;
					 scope.dae.rotation.y =  Math.random() * 360;

					//console.log('mine ', scope.dae)
					scope.loaded = true;
					scope.active = true;

					// var cubeGeometry = new THREE.CubeGeometry(300,300,300,1,1,1);
		   //          var wireMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe:true } );
		   //          scope.HitMesh = new THREE.Mesh( cubeGeometry, wireMaterial );
		   //          scope.HitMesh.visible = false;
		} );



		
	}

	return Mine
});
