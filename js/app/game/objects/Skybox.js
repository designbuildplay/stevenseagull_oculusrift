define(function (require) {
	
	var  	ColladaLoader 		= require('collada'),
		    Detector            = require('detector'),
		    THREE			    = require('threeCore');

	var Sky = function(){
		//console.log("BOMB!")
		var scope = this;
		this.mesh = {};

		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use
		//this.dae = new THREE.Object3D();//create an empty container
		this.dae = {};
		this.cube;

		this.axes = new THREE.AxisHelper(100);
		//scene.add( axes );
		
		var imagePrefix = "textures/dawnmountain-";
		//var directions  = ["xpos", "xpos", "xpos", "xpos", "xpos", "xpos"];
		var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
		var imageSuffix = ".png";
		var skyGeometry = new THREE.CubeGeometry( 30000, 30000, 30000 );	
		
		var materialArray = [];
		for (var i = 0; i < 6; i++)
			materialArray.push( new THREE.MeshBasicMaterial({
				map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
				side: THREE.BackSide
			}));
		var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
		this.skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
		//scene.add( skyBox );

		
	}

	return Sky
});

