define(function (require) {
		
	var  	ColladaLoader 		= require('collada'),
		    Detector            = require('detector'),
		    THREE			    = require('threeCore');
		    // this.dae = "213";


	var Leg = function(){

		var scope = this;
		this.dae = {};
		this.loaded = false; // to check if loaded
		this.active = false; // to tcheck if asset ready to use

		var loader = new THREE.OBJMTLLoader();
				loader.load( 'models/dae/ChickenLeg.obj', 'models/dae/ChickenLeg.mtl', function ( object ) {

					object.position.y = - 80;
					scope.dae = object;
					//scene.add( object );


					scope.dae.scale.x = scope.dae.scale.y = scope.dae.scale.z = 0.1;

					//console.log('mine ', scope.dae)
					scope.loaded = true;
					scope.active = true;

		} );


	}

	return Leg
});
