define(function (require) {

    "use strict";


        // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'), 
        
        OculusBridge    = require('OculusBridge'),
        //OrbitControls   = require('OrbitControls'),
        RiftCamera      = require('RiftCamera'),

        Mine               = require('mine'), 
        Leg                = require('leg'),  
        Box                = require('box'),  
        Crab               = require('crab'), 
        Sky                = require('sky'),  
        Cloud              = require('cloud'),  
        Sun                = require('sun'),
        PirateShip         = require('pirateship'),
        Island             = require('island'), 

        FirstPersonControls = require('fpControls'),
        stats           = require('stats'),
        THREE         = require('threeCore');

   // DEFINE THE GLOBALS ::::::::::::::::::::::::
          var renderer, camera;
          var scene, element;
          var ambient, point;
          var aspectRatio, windowHalf;
          var mouse, time;

          var controls;
          var clock;

          var useRift = false;

          var riftCam;

          var boxes = [];
          var core = [];
          var dataPackets = [];
          var scope;
          var ground, groundGeometry, groundMaterial;

          var bodyAngle;
          var bodyAxis;
          var bodyPosition;
          var viewAngle;

          var velocity;
          var oculusBridge;

          var wave, mesh2, dae, skin, texture, geometry, material;
          var worldWidth = 128, worldDepth = 128,
             worldHalfWidth = worldWidth / 2, worldHalfDepth = worldDepth / 2;
          var count = 0, count2 = 0;
          var mines = [];
          var crabs = [];
          var bonusboxes = [];
          var clouds = [];
          var islands = [];
          var islandCount = 5;
          var cloudCount = 30;
          var crabCount = 30;// number of crabs
          var mineCount = 30; // number of mines
          var boxCount = 15; // number of boxes
          var flySpeed = -15; // speed of movement
          var MovingCube, originPoint;
          var baddyDistance = 1005;
          var hit = false;
          // Map for key states
          var keys = [];
          for(var i = 0; i < 130; i++){
            keys.push(false);
          }

           // CONTENT :::::::::::::::::::::::::::::::::::
    
          var SeagullGame = function(){
            scope = this;
            this.collidableMeshList = [];

            this.init()
            this.animate()
          }


         SeagullGame.prototype.initScene = function(){
            clock = new THREE.Clock();
            mouse = new THREE.Vector2(0, 0);

            windowHalf = new THREE.Vector2(window.innerWidth / 2, window.innerHeight / 2);
            aspectRatio = (window.innerWidth/2) / (window.innerHeight/2);
            
            scene = new THREE.Scene();  

            camera = new THREE.PerspectiveCamera( 65, aspectRatio, 1, 20000 );

            camera.useQuaternion = true;
            camera.lookAt(scene.position);

           scene.fog = new THREE.FogExp2( 0xa6fbfa, 0.00025 );
           // scene.fog.color.setHSL( 1.51, 0.6, 0.6 );
            // Initialize the renderer
            renderer = new THREE.WebGLRenderer({antialias:true, alpha: false });
            renderer.setClearColor(0xa6fbfa, 1);
            renderer.setSize(window.innerWidth/2, window.innerHeight/2);

            element = document.getElementById('viewport');
            element.appendChild(renderer.domElement);
            $(element).addClass('fullScreen');

            stats = new Stats();
            stats.domElement.style.position = 'absolute';
            stats.domElement.style.top = '0px';
            element.appendChild( stats.domElement );

            controls = new THREE.FirstPersonControls( camera );
            controls.movementSpeed = 5700;
            controls.lookSpeed = 0.2


          }



          SeagullGame.prototype.initLights = function(){

            // CREATE SOME LIGHTS ::::::::::::::::::::::::::::::::::::::::::::::

            scene.add( new THREE.AmbientLight( 0x999999) );

            // LIGHT

            var light = new THREE.PointLight(0xffffff);
            light.position.set(0,250,0);
            scene.add(light);

            var lightRed = new THREE.PointLight(0xff215a);
            lightRed.position.set(400,250,200);
            scene.add(lightRed);

            // var lightBlue = new THREE.PointLight(0x021aa9);
            // light.position.set(0,-250,0);
            // scene.add(lightBlue);

          }


          SeagullGame.prototype.initGeometry = function(){

            // GAME ELEMENTS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

            geometry = new THREE.PlaneGeometry( 60000, 60000, worldWidth - 1, worldDepth - 1 );
            geometry.applyMatrix( new THREE.Matrix4().makeRotationX( - Math.PI / 2 ) );
            //geometry.dynamic = true;

            var i, j, il, jl;

            for ( i = 0, il = geometry.vertices.length; i < il; i ++ ) {
              geometry.vertices[ i ].y = 35 * Math.sin( i/2 );

            }

            geometry.computeFaceNormals();
            geometry.computeVertexNormals();
            var texture = THREE.ImageUtils.loadTexture( "textures/water.jpg" );
            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set( 5, 5 );

            material = new THREE.MeshBasicMaterial( { color: 0x3cafff, map: texture } )
            material.side = THREE.DoubleSide;
            wave = new THREE.Mesh( geometry, material );
            //wave.alpha = 0.5;
            scene.add( wave )
            wave.position.y = -150;

            // PLAYER :::::::::::::::::::::::::::::::::::::::::::::::::::::::::
            var cubeGeometry = new THREE.CubeGeometry(150,150,150,1,1,1);
            var wireMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe:true } );
            MovingCube = new THREE.Mesh( cubeGeometry, wireMaterial );
            MovingCube.position.set(0, 25.1, 0);
            scene.add( MovingCube );  
            MovingCube.visible = false;

            /// ADD THE CHICKEN LEG :::::::::::::::::::::::::::::::::::::::::::
            scope.legL = new Leg();
            scope.legR = new Leg();

            // ADD SOME MINES :::::::::::::::::::::::::::::::::::::::::::::::::
            for (var i = mineCount - 1; i >= 0; i--) {
              mines[i] = new Mine();

            };
            
            //ADD THE BOXES  ::::::::::::::::::::::::::::::::::::::::::::::::::
            for (var i = boxCount - 1; i >= 0; i--) {
              bonusboxes[i] = new Box()
              bonusboxes[i].texture.anisotropy = renderer.getMaxAnisotropy();
              scene.add(bonusboxes[i].mesh)
              scope.collidableMeshList.push(bonusboxes[i].mesh) // ADDS TO THE HIT LIST
              //console.log(boxes[i])
            };
            
            //CRABS WITH JET PACKS  :::::::::::::::::::::::::::::::::::::::::::
            for (var i = crabCount - 1; i >= 0; i--) {
              crabs[i] = new Crab()
              //console.log("hits ",scope.collidableMeshList.length)
            };

            //CREATE SOME CLOUDS :::::::::::::::::::::::::::::::::::::::::::::::
            for (var i = cloudCount - 1; i >= 0; i--) {
              clouds[i] = new Cloud();
            };
            
            //CREATE SOME PIRATES ::::::::::::::::::::::::::::::::::::::::::::::
            this.pirateship  = new PirateShip();

            //CREATE THE SKY  ::::::::::::::::::::::::::::::::::::::::::::::::::
            
            // this.sky = new Sky()
            // scene.add(this.sky.skyBox)
            this.sunny = new Sun()

            //ISLANDS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

            for (var i = islandCount - 1; i >= 0; i--) {
              islands[i] = new Island();
            };

          } 


          SeagullGame.prototype.init = function(){

            document.getElementById("toggle-render").addEventListener("click", function(){
              useRift = !useRift;
              onResize();
            });

            window.addEventListener('resize', onResize, false);

            time          = Date.now();
            bodyAngle     = 0;
            bodyAxis      = new THREE.Vector3(0, 1, 0);
            bodyPosition  = new THREE.Vector3(0, 15, 0);
            velocity      = new THREE.Vector3();

            scope.initScene();
            scope.initGeometry()
            scope.initLights()
            
            
            oculusBridge = new OculusBridge({
              "debug" : true,
              "onOrientationUpdate" : bridgeOrientationUpdated,
              "onConfigUpdate"      : bridgeConfigUpdated,
              "onConnect"           : bridgeConnected,
              "onDisconnect"        : bridgeDisconnected
            });
            oculusBridge.connect();

            riftCam = new THREE.OculusRiftEffect(renderer);
          }


          function onResize() {

            $('#blood').width(window.innerWidth)
            $('#blood').height(window.innerHeight)
            
            if(!useRift){
              windowHalf = new THREE.Vector2(window.innerWidth / 2, window.innerHeight / 2);
              aspectRatio = (window.innerWidth/2) / (window.innerHeight/2);
             
              camera.aspect = (window.innerWidth/2) / (window.innerHeight/2);
              camera.updateProjectionMatrix();
              controls.handleResize();

             
              renderer.setSize(window.innerWidth/2, window.innerHeight/2);
               $(element).addClass('fullScreen');
            } else {
              riftCam.setSize(window.innerWidth, window.innerHeight);
             $(element).removeClass('fullScreen');
            }
          }


          function bridgeConnected(){
            document.getElementById("logo").className = "";
          }

          function bridgeDisconnected(){
            document.getElementById("logo").className = "offline";
          }

          function bridgeConfigUpdated(config){
            console.log("Oculus config updated.");
            riftCam.setHMD(config);      
          }

          function bridgeOrientationUpdated(quatValues) {

            // Do first-person style controls (like the Tuscany demo) using the rift and keyboard.
            // TODO: Don't instantiate new objects in here, these should be re-used to avoid garbage collection.

            // make a quaternion for the the body angle rotated about the Y axis.
            var quat = new THREE.Quaternion();
            quat.setFromAxisAngle(bodyAxis, bodyAngle);

            // make a quaternion for the current orientation of the Rift
            var quatCam = new THREE.Quaternion(quatValues.x, quatValues.y, quatValues.z, quatValues.w);

            // multiply the body rotation by the Rift rotation.
            quat.multiply(quatCam);

            // Make a vector pointing along the Z axis and rotate it accoring to the combined look/body angle.
            var xzVector = new THREE.Vector3(0, 0, 1);
            xzVector.applyQuaternion(quat);

            // Compute the X/Z angle based on the combined look/body angle.  This will be used for FPS style movement controls
            // so you can steer with a combination of the keyboard and by moving your head.
            viewAngle = Math.atan2(xzVector.z, xzVector.x) + Math.PI;

            // Apply the combined look/body angle to the camera.
            camera.quaternion.copy(quat);
            // camera.translateZ(flySpeed)
          }

    

        SeagullGame.prototype.assetLoaded = function(asset){
            // CHECK AND LOAD OBJECTS ONCE READY ::::::::::::::::::::
            if ( asset.loaded == true ){
              asset.loaded = false
              scene.add(asset.dae)

              if(asset.HitMesh){
                 scene.add(asset.HitMesh) 
                 scope.collidableMeshList.push(asset.HitMesh) // ADDS TO THE HIT LIST
                 //console.log(scope.collidableMeshList)
              }
              //console.log('asset loadded.')
            }
        }

        SeagullGame.prototype.underWater = function(){
          // CHECK IF ABOVE WATER OR NOT:::::::::::::::::::::::
            if(camera.position.y >= wave.position.y - 20 ){
              renderer.setClearColor(0xa6fbfa, 1);
            }else{
              // var depthCount = 3678 + camera.position.y;
              // var d = Math.round(depthCount);
              // var depth = "0x2b" + d;
              // var dString = String(depth)
              // console.log(dString)
              // renderer.setClearColor(dString, 1);
              renderer.setClearColor(0x3cafff, 1);
            }
        }

        SeagullGame.prototype.playerHit = function(){
          if(hit == true){
            //hit = false;
            // camera.position.x = camera.position.x + Math.random() * 5
            // camera.position.y = camera.position.y + Math.random() * 5
            // //camera.position.z = camera.position.z + Math.random() * 15
           // TweenMax.set('#blood', {opacity:0.9} )
           // TweenMax.to('#blood', 0, {opacity:1 })
           $('#blood').css("display","block")
            TweenMax.to('#blood', 0.2, {opacity:0.8, })
            TweenMax.to('#blood', 0.3, {opacity:0, delay:0.5,  })
            console.log('hit')
            // TweenMax.to('#viewport', 1, {scaleX:2,scaleY:2,  delay:0.5, rotation:0, overwrite:true});
            hit = false;
          }
        }

        SeagullGame.prototype.animate = function(){
            var delta = clock.getDelta();
            time = clock.getElapsedTime() * 40;

            MovingCube.position.x = camera.position.x;
             MovingCube.position.y = camera.position.y;
              MovingCube.position.z = camera.position.z;

            scope.underWater()
            

              count += 0.09;
                
              for (var i = bonusboxes.length - 1; i >= 0; i--) {
                bonusboxes[i].mesh.rotation.x += 0.005;
                bonusboxes[i].mesh.rotation.y += 0.01; 
              };

              // positions the legs
              if ( scope.legL.active == true ){

                scope.legL.dae.rotation.y = camera.rotation.y;
                scope.legL.dae.position.x = camera.position.x - 200;
                scope.legL.dae.position.y = camera.position.y - 200 + (Math.random() * 10);
                scope.legL.dae.position.z = camera.position.z -30 + (Math.random() * 5);
              }

              if ( scope.legR.active == true ){
                scope.legR.dae.rotation.y = camera.rotation.y;
                scope.legR.dae.position.x = camera.position.x - 100;
                scope.legR.dae.position.y = camera.position.y - 200 + (Math.random() * 10);
                scope.legR.dae.position.z = camera.position.z + 40 + (Math.random() * 5);
              }

               if ( scope.pirateship.active == true ){
                scope.pirateship.dae.rotation.y = Math.sin(count) * 0.0002;
                scope.pirateship.dae.position.y = Math.cos(count) *  0.02;
                
              }

              for (var i = mines.length - 1; i >= 0; i--) {
                if ( mines[i].active == true ){
                  mines[i].dae.rotation.x += 0.003;
                  mines[i].dae.rotation.y += 0.01;
                }
              };

              for (var i = crabs.length - 1; i >= 0; i--) {
                if ( crabs[i].active == true ){
                  // CREATES THE BOUNCE
                  crabs[i].dae.position.y +=  Math.cos(count) * crabs[i].speed;
                  
                  crabs[i].HitMesh.position.x = crabs[i].dae.position.x 
                  crabs[i].HitMesh.position.y = crabs[i].dae.position.y + 100
                  crabs[i].HitMesh.position.z = crabs[i].dae.position.z 

                  //move it hit
                  if(crabs[i].HitMesh.scale.x == 0.1){
                       crabs[i].HitMesh.scale.x = 1;
                       crabs[i].HitMesh.scale.y = 1;
                       crabs[i].HitMesh.scale.z = 1;
                      crabs[i].dae.position.x = Math.random() * 10000 + 1500
                      crabs[i].dae.position.z = Math.random() * 10000 + 1500
                      crabs[i].dae.position.y = Math.random() * 800 + 100    
                  }

                  if(i == 0){
                   scope.chasePlayer(crabs) //MOVES THE ENEMY TO PLAYER
                  // console.log("chase")
                  }
                }

               
              };


              for (var i = clouds.length - 1; i >= 0; i--) {
                if ( clouds[i].active == true ){
                  var num = clouds[i].dae.scale.z;
                  clouds[i].dae.position.z =  clouds[i].dae.position.z + (1*num);
                  if(clouds[i].dae.position.z  > 10000){
                    clouds[i].dae.position.z = -10000
                  }
                }
              };



          // COLLISION DETECTION :::::::::::::::::::::::::::::::::::::::::::::::::::::::
          originPoint = MovingCube.position.clone();

          for (var vertexIndex = 0; vertexIndex < MovingCube.geometry.vertices.length; vertexIndex++)
          {   
            var localVertex = MovingCube.geometry.vertices[vertexIndex].clone();
            var globalVertex = localVertex.applyMatrix4( MovingCube.matrix );
            var directionVector = globalVertex.sub( MovingCube.position );
            
            var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
            var collisionResults = ray.intersectObjects( scope.collidableMeshList );
            if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() ) 
            {
               // a collision occurred... do something...
               //console.log(collisionResults[0].object)
               collisionResults[0].object.scale.x = 0.1;
               collisionResults[0].object.scale.y = 0.1;
               collisionResults[0].object.scale.z = 0.1;

               hit = true
            }       
          }

          scope.playerHit() // controls hit action

          if(useRift){
            //flySpeed = flySpeed - 0.05
            camera.translateZ(flySpeed)
          }
          else{
            scope.render();
          }
          
          stats.update();

            if(scope.render()){
              requestAnimationFrame(scope.animate);  
            }
          }

      SeagullGame.prototype.chasePlayer = function(object){

        this.object = object;
        //CHASE PLAYER ::::::::::::::::::::::::::::::::::::::::
        // Rotate the enemy to face the player
        for (var i = this.object.length - 1; i >= 0; i--) {
          var rotation = Math.atan2(camera.position.z - this.object[i].dae.position.z, camera.position.x - this.object[i].dae.position.x);
                          
          this.object[i].dae.rotation.y = Math.cos(-rotation *2) * this.object[i].speed; //radians

          var distanceZ = Math.sin(rotation) * this.object[i].speed / 25;

          //if(distanceZ < baddyDistance ){
             // Move in the direction we're facing
             this.object[i].dae.position.z += Math.sin(rotation) * this.object[i].speed ;
            this.object[i].dae.position.x += Math.cos(rotation) * this.object[i].speed ;
         // }
         
        };
        
      }
         
       SeagullGame.prototype.render = function(){
         
        // SET THE WAVE SPEED AND HEIGHT :::::::::::::::::::::::
        var delta = clock.getDelta(),
          time = clock.getElapsedTime() * 40;


        for ( var i = 0, l = geometry.vertices.length; i < l; i ++ ) {
          geometry.vertices[ i ].y = 105 * Math.sin( i / 5 + ( time + i ) / 7 );
        }

        wave.geometry.verticesNeedUpdate = true;

        // CHECK AND LOAD OBJECTS ONCE READY ::::::::::::::::::::
        // scope.assetLoaded(scope.legL)
        // scope.assetLoaded(scope.legR)

        scope.assetLoaded(scope.sunny)
        scope.assetLoaded(scope.pirateship)

        for (var i = mines.length - 1; i >= 0; i--) {
          scope.assetLoaded(mines[i])
        };

        for (var i = crabs.length - 1; i >= 0; i--) {
          scope.assetLoaded(crabs[i])
        };

        for (var i = clouds.length - 1; i >= 0; i--) {
          scope.assetLoaded(clouds[i])
        };

        for (var i = islands.length - 1; i >= 0; i--) {
          scope.assetLoaded(islands[i])
        };


        //UPDATE CONTROL 

        // RENDERS THE ELEMENTS TO THE DEVICE :::::::::::::::::::
            try{
              if(useRift){
                riftCam.render(scene, camera);
              }else{
                controls.update(delta);
                renderer.render(scene, camera);
              }  
            } catch(e){
              console.log(e);
              if(e.name == "SecurityError"){
                crashSecurity(e);
              } else {
                crashOther(e);
              }
              return false;
            }
            return true;

        }


    function crashSecurity(e){
            oculusBridge.disconnect();
            document.getElementById("viewport").style.display = "none";
            document.getElementById("security_error").style.display = "block";
          }

    function crashOther(e){
            oculusBridge.disconnect();
            document.getElementById("viewport").style.display = "none";
            document.getElementById("generic_error").style.display = "block";
            document.getElementById("exception_message").innerHTML = e.message;
    }


     return SeagullGame
})