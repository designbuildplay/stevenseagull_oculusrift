
// Script loader (RequireJS) ==========================

require.config({

  //'baseUrl': '/DBP/dbp_phase2/dbp_p2_build/js/', // THE BASE URL

   paths: {
        /* jQuery */
        'jquery': 'lib/jquery-1.7.2.min',
        'underscore': 'lib/underscore-min',
        'backbone': 'lib/backbone-min',

       

        'tweenmax': 'lib/gs/TweenMax.min',
        'ease': 'lib/gs/easing/EasePack.min',
        'pixi': 'lib/pixi/pixi.dev',
        'threeCore': 'lib/three.min',
        'collada': 'lib/ColladaLoader',
        'OBJLoader': 'lib/OBJLoader',
        'MTLLoader': 'lib/MTLLoader',
        'OBJMTLLoader': 'lib/OBJMTLLoader',
        'fpControls': 'utils/FirstPersonControls',
        'detector': 'lib/Detector',
        'stats': 'lib/stats.min',

        /* APP MODULES */
        'app': 'app',
        'game': 'app/game/Game',
        'mine': 'app/game/objects/Mine',
        'box': 'app/game/objects/Box',
        'crab': 'app/game/objects/Crab',
        'leg': 'app/game/objects/Leg',
        'sky': 'app/game/objects/Skybox',
        'cloud': 'app/game/objects/Cloud',
        'sun': 'app/game/objects/Sun',
        'pirateship': 'app/game/objects/PirateShip',
        'island': 'app/game/objects/Island',

        /* OCULUS LIBS */
        
        'OculusBridge': 'lib/oculus/OculusBridge.min',
        'OrbitControls': 'lib/oculus/OrbitControls',
        'RiftCamera': 'lib/oculus/RiftCamera',
        'seagullGame': 'app/game/SeagullGame',
        'connect': 'app/game/connect',

        'model.baddy' : 'app/models/baddy',
        'collection.questions' : 'app/collections/questions',
        'view.game': 'app/views/game',
        'view.menu': 'app/views/menu',
        'view.end': 'app/views/ended',

         /* Router */
        'router': 'router',
    },

    shim: {

        'underscore': {
          'exports': '_'
        },

        'backbone': {
          'deps': ['jquery', 'underscore'],
          'exports': 'Backbone'
        },

        'fastclick': {
            exports: 'FastClick'
        },

        "tweenmax" : 
         {deps: ["jquery"],
          exports :"TweenMax"
         },

         'pixi': {
          exports: 'PIXI'
        },

         // --- Use shim to mix together all THREE.js subcomponents
        'threeCore': { exports: 'THREE' },
        'TrackballControls': { deps: ['threeCore'], exports: 'THREE' },
        // --- end THREE sub-components
        'detector': { exports: 'Detector' },
        'stats': { exports: 'Stats' },
        'OculusBridge': { exports: 'OculusBridge' },
   }
});


require([
  // Load our app module and pass it to our definition function
  
  'app',
  'jquery', 
  'backbone',
  //'router'

], function(App, $, Backbone){
  // The "app" dependency is passed in as "App"
    App.initialize();
    
});

